import React from 'react';
import Swipeable from 'react-swipeable';

import { 
	Carousel, 
	CarouselItem, 
	CarouselControl,
	CarouselIndicators,
	CarouselCaption
} from 'reactstrap';

class BgImg extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			error: null,
			isLoaded: false,
			projects: [],
			activeIndex: 0,
			winWidth: 0,
			winHeight: 0
		};

		this.updateWindowDimensions = this.updateWindowDimensions.bind(this);

		this.next = this.next.bind(this);
		this.previous = this.previous.bind(this);
		this.goToIndex = this.goToIndex.bind(this);
		this.onExiting = this.onExiting.bind(this);
		this.onExited = this.onExited.bind(this);
	}


	onExiting() {
		this.animating = true;
	}

	onExited() {
		this.animating = false;
	}

	next() {
		if (this.animating) return;
		const nextIndex = this.state.activeIndex === this.state.projects.length - 1 ? 0 : this.state.activeIndex + 1;
		this.setState({ activeIndex: nextIndex });
	}

	previous() {
		if (this.animating) return;
		const nextIndex = this.state.activeIndex === 0 ? this.state.projects.length - 1 : this.state.activeIndex - 1;
		this.setState({ activeIndex: nextIndex });
	}

	goToIndex(newIndex) {
		if (this.animating) return;
		this.setState({ activeIndex: newIndex });
	}

	updateWindowDimensions() {
		this.setState({ winWidth: window.innerWidth, winHeight: window.innerHeight });
	}

	componentDidMount() {
		window.addEventListener('resize', this.updateWindowDimensions);

		fetch("http://fmolina.com.br:3184/projects")
			.then(res => res.json())
			.then(
				(result) => {
					result.forEach((project) => {
						if(!project.photos) project.photos = [];
						if(!project.cover || project.cover < 0 || project.cover > project.photos.length) project.cover = 0;
					});
					this.setState({
						isLoaded: true,
						projects: result,
						winWidth: window.innerWidth, 
						winHeight: window.innerHeight
					});
				},
				(error) => {
					this.setState({
						isLoaded: true,
						error,
						winWidth: window.innerWidth, 
						winHeight: window.innerHeight
					});
				}
			);
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { error, isLoaded, projects, activeIndex, winWidth, winHeight } = this.state;
		if (error) {
			return <div>Error: {error.message}</div>;
		} else if (!isLoaded) {
			return <div>Loading...</div>;
		} else {
			var pWindow = winWidth/(winWidth+winHeight);

			const photos = [];
			projects.forEach((project) => {
				var photo = {};
				if(project.cover < project.photos.length)
					photo = project.photos[project.cover];

				photo.maxWidth = pWindow > (photo.width/(photo.width+photo.height));

				photos.push(
					<CarouselItem onExiting={this.onExiting}
								  onExited={this.onExited}
								  key={project._id}>
						<img src={photo.src} alt={project.projecName} className={ (photo.maxWidth) ? "max-width":"max-height" } />
						<CarouselCaption captionText={project.projectDesc} captionHeader={project.projectName} />
					</CarouselItem>
				);
			});

			return (
				<Swipeable onSwipingLeft={this.next} onSwipingRight={this.previous}>
				<Carousel
						interval="10000000"
						activeIndex={activeIndex}
						next={this.next}
						previous={this.previous}>
					<CarouselIndicators items={photos} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
						{photos}
					<CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
					<CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
				</Carousel>
				</Swipeable>
			);
		}
	}
}

export default BgImg;
