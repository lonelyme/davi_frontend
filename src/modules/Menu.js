import React from 'react';
import CX from 'classnames';
import { instanceOf } from 'prop-types';
import { Link } from 'react-router-dom';
import { withCookies, Cookies } from 'react-cookie';
import { Collapse, 
         Navbar, 
         NavbarToggler, 
         NavbarBrand, 
         Nav, 
         NavItem, 
         NavLink } from 'reactstrap';

class Menu extends React.Component {
	static propTypes = {
		cookies: instanceOf(Cookies).isRequired
	};

	constructor(props) {
		super(props);

		this.checkLogin = this.checkLogin.bind(this);
		this.logout = this.logout.bind(this);
		this.toggleNavbar = this.toggleNavbar.bind(this);

		this.state = {
			collapsed: true,
			token: null
		};
	}
	
	componentWillMount() {
		this.checkLogin();
	}

	toggleNavbar() {
		this.setState({
			collapsed: !this.state.collapsed
		});
	}
	
	checkLogin() {
		const { cookies } = this.props;

		var token = cookies.get('token');	
		if(token) {
			this.setState({ token: token })
		}
	}

	logout() {
		const { cookies } = this.props;

		var token = cookies.get('token');	
		fetch('http://fmolina.com.br:3184/logout/', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': token.token
			}
		})
		.then(() => {
			cookies.remove('token', { path: '/' });
			this.setState({
				token: null,
				collapsed: !this.state.collapsed
			});
		});
	}
	
	render() {
		return (
			<Navbar className="navbar-size" color="light" light expand="md">
				<NavbarBrand tag={Link} to="/" className={CX("mr-auto", "navbar-brand-size")} >SALVADORDAVI</NavbarBrand>
				<NavbarToggler onClick={this.toggle} />
				<Collapse isOpen={this.state.isOpen} navbar>
					<Nav className="ml-auto" navbar>
						<NavItem>
							<NavLink tag={Link} to="/">projects</NavLink>
						</NavItem>
						<NavItem>
							<NavLink tag={Link} to="/about">about</NavLink>
						</NavItem>
						<NavItem>
							<NavLink>contact</NavLink>
						</NavItem>
					</Nav>
				</Collapse>
			</Navbar>
		);
	}
}

export default withCookies(Menu);
