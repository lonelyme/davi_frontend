import React from 'react';

import { instanceOf } from 'prop-types';
import { withCookies, Cookies } from 'react-cookie';
import { Link } from 'react-router-dom';

import { 
	Alert,
	Card,
	CardHeader,
	CardBody
} from 'reactstrap';

class Login extends React.Component {
	static propTypes = {
		cookies: instanceOf(Cookies).isRequired
	};

	constructor(props) {
		super(props);
		this.state = {
			error: false,
			errorMessage: "",
			token: null,
			projects: null
		};

		this.requestProjects = this.requestProjects.bind(this);
	}

	componentWillMount() {
		const { cookies } = this.props;

		var token = cookies.get('token');
		this.requestProjects(token);
	}

	requestProjects(token) {
		fetch('http://fmolina.com.br:3184/projects', {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
		.then(res => res.json())
		.then(
			(result) => {
				if(result.error) {
					this.setState({ 
						error: true,
						errorMessage: result.error,
						token: token,
						projects: []
					});
				} else {
					this.setState({ 
						error: false,
						projects: result,
						token: token
					});
				}
			},
			(error) => {
				this.setState({ 
					token: token, 
					error: true,
					errorMessage: error,
					projects: []
				});

			}
		);
	}

	render () {
		const { error, errorMessage, projects, token } = this.state;

		if(!(projects || error)) {
			return (<div> Loading... </div>);
		}

		if(!token) {
			window.location.href = "/";
		}

		const projs = []
		for(var i=0; i < projects.length; i++) {
			
			if(!projects[i].cover || projects[i].cover<0 || projects[i].cover>=projects[i].photos.length) 
				projects[i].cover = 0;
			const projectURL = "/manage/projects/" + projects[i]._id;
			projs.push(
				<Link to={projectURL} key={i} >
				<Card>
					<CardBody>
						<img src={projects[i].photos[projects[i].cover].src} alt={projects[i].projectName} />
					</CardBody>
					<CardHeader>{projects[i].projectName}</CardHeader>
				</Card>
				</Link>
			);
		}

		return (
			<Card className="" >
				<CardHeader tag="h5">Projects</CardHeader>
				<CardBody className="mng-projects-body">
					<Alert color="danger" isOpen={error}>
						{errorMessage}
					</Alert>
					{projs}
					<div className="bottom-space" ></div>
				</CardBody>
			</Card>
		);
	}
}

export default withCookies(Login);
