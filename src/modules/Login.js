import React from 'react';

import $ from 'jquery';

import { instanceOf } from 'prop-types';
import { withCookies, Cookies } from 'react-cookie';

import { 
	Button, 
	Alert,
	Card, 
	CardHeader, 
	CardBody,	
	Form, 
	FormGroup, 
	Label, 
	Input
} from 'reactstrap';

class Login extends React.Component {
	static propTypes = {
		cookies: instanceOf(Cookies).isRequired
	};

	constructor(props) {
		super(props);
		this.state = {
			error: false,
			errorMessage: "",
			disabled: false,
			user: "",
			password: "",
			token: null
		};

		this.requestLogin = this.requestLogin.bind(this);
		this.submitOnEnter = this.submitOnEnter.bind(this);
		this.userChange = this.userChange.bind(this);
		this.passwordChange = this.passwordChange.bind(this);
	}

	componentWillMount() {
		const { cookies } = this.props;

		var token = cookies.get('token');	
		if(token) {
			this.setState({ token: token })
		}
	}

	componentDidMount() {
		$(document).keypress(this.submitOnEnter);
	}

	componentWillUnmount() {
		$(document).off("keypress");
	}

	submitOnEnter(e) {
		var checkEnter=(e.which===13 ? true : false);

		if (checkEnter) 
			this.requestLogin();
	}

	userChange(event) {
		this.setState({user: event.target.value});
	}

	passwordChange(event) {
		this.setState({password: event.target.value});
	}

	requestLogin() {
		const { cookies } = this.props;

		this.setState({ 
			disabled: true,
			error: null
		});

		var credentials = {
			user: this.state.user,
			password: this.state.password
		};

		fetch('http://fmolina.com.br:3184/login', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(credentials)
		})
		.then(res => res.json())
		.then(
			(result) => {
				if(result.error) {
					this.setState({ 
						disabled: false,
						error: true,
						errorMessage: result.error
					});
				} else {
					cookies.set('token', result, { path: '/' });
					this.setState({ token: result })
				}
			},
			(error) => {
				this.setState({ 
					disabled: false,
					error: true,
					errorMessage: error
				});

			}
		);
	}

	render () {
		if(this.state.token) {
			window.location.href = "/";
		}

		return (
		<div>
			<Card className="login-card" >
				<CardHeader tag="h3">Login</CardHeader>
				<CardBody>
					<Form>
						<Alert color="danger" isOpen={this.state.error}>
							{this.state.errorMessage}
						</Alert>
						<FormGroup>
							<Label for="email">Email</Label>
							<Input type="email" name="email" id="email" disabled={this.state.disabled} onChange={this.userChange} />
						</FormGroup>
						<FormGroup>
							<Label for="password">Password</Label>
							<Input type="password" name="password" id="password" disabled={this.state.disabled} onChange={this.passwordChange} />
						</FormGroup>
						<FormGroup>
							<Button onClick={this.requestLogin} disabled={this.state.disabled} >Submit</Button>
						</FormGroup>
					</Form>
				</CardBody>
			</Card>
		</div>
		);
	}
}

export default withCookies(Login);
