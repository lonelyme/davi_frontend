import React from 'react';

import { Button } from 'reactstrap';
import { instanceOf } from 'prop-types';
import { Redirect } from 'react-router-dom'
import { withCookies, Cookies } from 'react-cookie';

import $ from 'jquery';

import { 
	Alert,
	Card,
	CardHeader,
	CardBody
} from 'reactstrap';

class Login extends React.Component {
	static propTypes = {
		cookies: instanceOf(Cookies).isRequired
	};

	constructor(props) {
		super(props);
		this.state = {
			error: false,
			errorMessage: "",
			token: null,
			redirect: false,
			projectId: props.match.params.id,
			projec: null,
			selectedPhoto: 0
		};
		
		this.saveProject = this.saveProject.bind(this);
		this.requestProject = this.requestProject.bind(this);
		this.requestRemovePhoto = this.requestRemovePhoto.bind(this);
		this.getImageAndPost = this.getImageAndPost.bind(this);

		this.setCover = this.setCover.bind(this);
		this.selectPhoto = this.selectPhoto.bind(this);
		this.removePhoto = this.removePhoto.bind(this);
		this.toggleVisible = this.toggleVisible.bind(this);

		this.movePhoto = this.movePhoto.bind(this);
		this.moveRight = this.moveRight.bind(this);
		this.moveLeft = this.moveLeft.bind(this);
		this.drop = this.drop.bind(this);

		this.selectImages = this.selectImages.bind(this);
		this.saveSelectedPhotos = this.saveSelectedPhotos.bind(this);
	}

	componentWillMount() {
		const { cookies } = this.props;

		var token = cookies.get('token');
		this.requestProject(token, this.state.projectId);
	}

	selectImages() {
		$('#fileinput').trigger('click');
	}

	getImageAndPost(file) {
		const { token, projectId, project } = this.state;

		var _URL = window.URL || window.webkitURL;
		
		var img = new Image();
		img.src = _URL.createObjectURL(file);
		img.onload = function() {

			var formData = new FormData();
			formData.append("userfile", file);
			formData.append("width", img.width);
			formData.append("height", img.height);
			formData.append("heightasd", "teste do capeta");

			fetch('http://fmolina.com.br:3184/projects/'+projectId+'/image', {
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Authorization': token.token
				},
				body: formData
			}).then(
				response => response.json() // if the response is a JSON object
			).then(
				result => {
					if(result.error) {
						this.setState({ 
							error: true,
							errorMessage: result.error
						});
					} else {
						project.photos.push(result);
						this.setState({ project: project });
					}
				}
			).catch(
				error => console.log(error) // Handle the error response object
			);
		};
	}

	saveSelectedPhotos() {
		var fileList = $('#fileinput')[0].files;
		for (var i=0; i<fileList.length; i++) {
			this.getImageAndPost(fileList[i]);
		}
	}

	requestProject(token, id) {
		fetch('http://fmolina.com.br:3184/projects/'+id, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
		.then(res => res.json())
		.then(
			(result) => {
				if(result.error) {
					this.setState({ 
						error: true,
						errorMessage: result.error,
						project: { photos: [] },
						token: token
					});
				} else {
					this.setState({ 
						error: false,
						project: result,
						token: token
					});
				}
			},
			(error) => {
				this.setState({ 
					token: token,
					project: { photos: [] },
					error: true,
					errorMessage: error
				});

			}
		);
	}
	
	saveProject() {
		const { token, project } = this.state;
		fetch('http://fmolina.com.br:3184/projects/', {
			method: 'PUT',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': token.token
			},
			body: JSON.stringify(project)
		})
		.then(res => res.json())
		.then(
			(result) => {
				if(result.error) {
					this.setState({ 
						error: true,
						errorMessage: result.error
					});
				} else {
					this.setState({ 
						redirect: true
					});
				}
			},
			(error) => {
				this.setState({ 
					error: true,
					errorMessage: error
				});
			}
		);
	}

	toggleVisible(e) {
		e.preventDefault();

		var { project } = this.state;
		var index = parseInt(e.target.getAttribute('index'),10);
		
		project.photos[index].visible = !project.photos[index].visible;

		this.setState({ project: project });
	}

	requestRemovePhoto(e) {
		e.preventDefault();
		
		const { token, projectId, project } = this.state;
		var index = parseInt(e.target.getAttribute('index'),10);
		
		fetch('http://fmolina.com.br:3184/projects/'+projectId+'/image', {
			method: 'DELETE',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': token.token
			},
			body: JSON.stringify(project.photos[index])
		})
		.then(res => res.json())
		.then(
			(result) => {
				if(result.error) {
					this.setState({ 
						error: true,
						errorMessage: result.error
					});
				} else {
					this.removePhoto(index);
				}
			},
			(error) => {
				this.setState({ 
					error: true,
					errorMessage: error
				});
			}
		);
	}

	removePhoto(index) {

		var { project, selectedPhoto } = this.state;
		
		if(0<=index && index<project.photos.length) {
			project.photos.splice(index, 1);

			if(selectedPhoto===project.photos.length)
				selectedPhoto--;

			if(project.cover===project.photos.length)
				project.cover--;
		}

		this.setState({ project: project, selectedPhoto: selectedPhoto });
	}

	selectPhoto(e) {
		e.preventDefault();
		this.setState({ selectedPhoto: parseInt(e.target.getAttribute('index'),10) });
	}

	allowDrop(ev) {
		ev.preventDefault();
	}

	drag(ev) {
		var data = ev.target.id;
		ev.dataTransfer.setData("text", data);
	}

	movePhoto(orig, dest) {
		var { project, selectedPhoto } = this.state;
		
		var pLength = project.photos.length;
		if(orig < 0 || orig >= pLength || dest < 0 || dest >=pLength)
			return;

		var i = orig;
		var photoOrig = project.photos[orig];
		if(orig<dest) {
			if(orig<=selectedPhoto && selectedPhoto<=dest) {
				if(orig===selectedPhoto) selectedPhoto=dest;
				else selectedPhoto--;
			}

			if(orig<=project.cover && project.cover<=dest) {
				if(orig===project.cover) project.cover=dest;
				else project.cover--;
			}

			while(i<pLength-1 && i<dest) {
				project.photos[i] = project.photos[++i];
			}
		} else {
			if(dest<=selectedPhoto && selectedPhoto<=orig) {
				if(orig===selectedPhoto) selectedPhoto=dest;
				else selectedPhoto++;
			}

			if(dest<=project.cover && project.cover<=orig) {
				if(orig===project.cover) project.cover=dest;
				else project.cover++;
			}

			while(i>0 && i>dest) {
				project.photos[i] = project.photos[--i];
			}
		}
		
		project.photos[dest] = photoOrig;
		
		this.setState({ project: project, selectedPhoto: selectedPhoto });
	}
	
	drop(ev) {
		ev.preventDefault();

		var dest = parseInt(ev.target.getAttribute('index'), 10);
		var orig = parseInt(ev.dataTransfer.getData('text'), 10);
		this.movePhoto(orig, dest);
	}

	moveRight(e) {
		e.preventDefault();

		var orig = parseInt(e.target.getAttribute('index'), 10);
		var dest = orig+1;
		this.movePhoto(orig, dest);
	}

	moveLeft(e) {
		e.preventDefault();

		var orig = parseInt(e.target.getAttribute('index'), 10);
		var dest = orig-1;
		this.movePhoto(orig, dest);
	}

	setCover(e) {
		e.preventDefault();
		
		var { project } = this.state;

		var orig = parseInt(e.target.getAttribute('index'), 10);
		if(0<=orig && orig<project.photos.length) 
			project.cover = orig;

		this.setState({ project: project });
	}

	render () {
		const { error, errorMessage, redirect, project, token, selectedPhoto } = this.state;

		if(!(project || error)) {
			return (<div> Loading... </div>);
		}

		if(!token) {
			window.location.href = "/";
		}

		if(redirect) {
			return (<Redirect to="/manage/projects" />);
		}
		
		const photos = []
		if(!project.cover || project.cover<0 || project.cover>=project.photos.length) 
			project.cover = 0;
		
		var base = 'http://fmolina.com.br:3184/';
		for(var i=0; i < project.photos.length; i++) {
			var imgSrc = project.photos[i].src;
			imgSrc = imgSrc.startsWith("http") ? imgSrc : base+imgSrc;

			photos.push(
				<div key={i} index={i} onDrop={this.drop} onDragOver={this.allowDrop} >
					<div className="buttons-left">
						<Button color="secondary" onClick={this.toggleVisible} index={i} >
							<i className={(project.photos[i].visible) ? "fas fa-eye":"fas fa-eye-slash"} ></i>
						</Button>
						<Button color="secondary" onClick={this.moveLeft} index={i}>
							<i className="fas fa-angle-double-left" ></i>
						</Button>
						<Button color="secondary">
							<i className="fas fa-pause-circle invisible"></i>
						</Button>
					</div>
					<img id={i} src={imgSrc} alt={project.projectName} index={i} onClick={this.selectPhoto} draggable="true" onDragStart={this.drag} className={(project.photos[i].visible) ? "":"non-visible"} />
					<div className="buttons-right">
						<Button color="danger" onClick={this.requestRemovePhoto} index={i} >
							<i className="fas fa-times" ></i>
						</Button>
						<Button color="secondary" onClick={this.moveRight} index={i} >
							<i className="fas fa-angle-double-right" ></i>
						</Button>
						<Button color="secondary" onClick={this.setCover} index={i} >
							<i className={(i===project.cover) ? "fas fa-image selected":"fas fa-image"} ></i>
						</Button>
					</div>
				</div>
			);
		}

		var selectedSrc = project.photos[selectedPhoto].src;
		selectedSrc = selectedSrc.startsWith('http') ? selectedSrc : base+selectedSrc;
		
		return (
			<Card className="" >
				<CardHeader>
					<h4>{project.projectName}</h4>
					<i className="fas fa-edit"></i>

					<Button color="primary" className="right" onClick={this.saveProject}>Save</Button>
					<Button color="secondary" className="right add-photos" onClick={this.selectImages} >
						<i className="fas fa-plus-square"></i>
					</Button>
					<input id="fileinput" type="file" multiple="true" accept="image/*" className="input-images" onChange={this.saveSelectedPhotos} />
				</CardHeader>
				<CardBody className="mng-projects-body">
					<Alert color="danger" isOpen={error}>
						{errorMessage}
					</Alert>
					<div className="selected-area">
						<div className="selected-photo">
							<img src={selectedSrc} alt="selected" />
						</div>
						<div className="seleced-description">
							{project.photos[selectedPhoto].description}
						</div>
					</div>
					<div className="manage-photos">
						{photos}
					</div>
				</CardBody>
			</Card>
		);
	}
}

export default withCookies(Login);
