import React from 'react';
import { Route, Switch } from 'react-router-dom';

import '../css/Barlow.css';
import '../css/App.css';

import Menu     from './Menu';
import Home     from './Home';
import About    from './About';
import Login    from './Login';

import MngProject    from './MngProject';
import MngProjects    from './MngProjects';


class App extends React.Component {
	render() {
		return (
			<div id="App">
				<Menu />
				<div className="App">
					<Switch>
						<Route exact path="/" component={Home} />
						<Route path="/about" component={About} />
						<Route path="/login" component={Login}  />

						<Route exact path="/manage/projects" component={MngProjects}  />
						<Route path="/manage/projects/:id" component={MngProject}  />
					</Switch>
				</div>
			</div>
		);
	}
}

export default App;

