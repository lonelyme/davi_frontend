import React from 'react';
import ReactDOM from 'react-dom';

import { BrowserRouter as Router, Route } from "react-router-dom";

import { CookiesProvider } from 'react-cookie';

import 'bootstrap/dist/css/bootstrap.css';
import App from './modules/App';

import registerServiceWorker from './registerServiceWorker';

ReactDOM.render((
	<CookiesProvider>
		<Router>
			<Route path="/" component={App} />
		</Router>
	</CookiesProvider>
), document.getElementById('root'));

registerServiceWorker();
